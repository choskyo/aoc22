﻿var input = File.ReadAllLines("./input");

var root = new Dir { Id = "/" };

var cwd = root;

for (var i = 0; i < input.Length; i++)
{
    if (input[i][0] == '$')
    {
        var cmd = input[i][2..4];
        if (cmd == "cd")
        {
            var path = input[i][5..];

            switch (path)
            {
                case "/":
                    cwd = root;
                    break;
                case "..":
                    cwd = cwd.Parent!;
                    break;
                default:
                    if (!cwd.SubDirs.ContainsKey(path))
                    {
                        cwd.SubDirs.Add(path, new Dir() { Id = path, Parent = cwd });
                    }
                    cwd = cwd.SubDirs[path];
                    break;
            }
        }
        else if (cmd == "ls")
        {
            i++;
            while (i < input.Length && input[i][0] != '$')
            {
                if (int.TryParse(input[i][0].ToString(), out _))
                {
                    cwd.SumFileSizes += int.Parse(input[i].Split(' ')[0]);
                }
                i++;
            }
            i--;
        }
    }
}

(int, int) SolveFirst(Dir dir)
{
    var size = dir.SumFileSizes;
    var total = 0;

    foreach (var (_, sub) in dir.SubDirs)
    {
        var (subSize, subTotal) = SolveFirst(sub);
        size += subSize;
        total += subTotal;
    }

    if (size <= 100000)
    {
        total += size;
    }

    return (size, total);
}

var (_, p1Res) = SolveFirst(root);

Console.WriteLine($"p1: {p1Res}");

List<int> GetTotalDirs(Dir dir)
{
    var size = dir.SumFileSizes;
    var sln = new List<int>();

    foreach (var (_, sub) in dir.SubDirs)
    {
        var subSizes = GetTotalDirs(sub);
        size += subSizes.Last();
        sln = sln.Concat(subSizes).ToList();
    }

    sln.Add(size);

    return sln;
}

var totals = GetTotalDirs(root);
var target = totals.Last() - 40000000;

totals.Sort();

var p2Res = totals.First(size => size >= target);

Console.WriteLine($"p2: {p2Res}");

class Dir
{
    public string Id { get; set; } = "";
    public int SumFileSizes { get; set; }
    public Dir? Parent { get; set; }
    public Dictionary<string, Dir> SubDirs { get; set; } = new Dictionary<string, Dir>();
};