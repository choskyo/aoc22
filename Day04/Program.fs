﻿open System.IO
open System

let input = File.ReadAllLines "./input"

let getMinMax (pairStr: string) =
    let split = pairStr.Split '-'
    (int split[0], int split[1])

let getRanges (pairStr: string) =
    let ranges = pairStr.Split ','
    let (fstMin, fstMax) = getMinMax ranges[0]
    let (sndMin, sndMax) = getMinMax ranges[1]
    (fstMin, fstMax, sndMin, sndMax)

let p1Mapper (pairStr: string) =
    let (fstMin, fstMax, sndMin, sndMax) = getRanges pairStr

    if fstMin >= sndMin && fstMax <= sndMax then 1
    else if sndMin >= fstMin && sndMax <= fstMax then 1
    else 0

let p1Res = input |> Array.map p1Mapper |> Array.sum

printf "p1: %d\n" p1Res

let p2Mapper (pairStr: string) =
    let (fstMin, fstMax, sndMin, sndMax) = getRanges pairStr

    if (Math.Max(fstMin, sndMin)) <= (Math.Min(fstMax, sndMax)) then
        1
    else
        0

let p2Res = input |> Array.map p2Mapper |> Array.sum

printf "p2: %d\n" p2Res
