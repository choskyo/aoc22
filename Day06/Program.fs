﻿open System.IO

let input = File.ReadAllLines "./input" |> Array.exactlyOne

let findFirstMessage (packet: string) (msgSize: int) : int =
    packet
    |> Seq.windowed msgSize
    |> Seq.findIndex (fun x -> (Seq.distinct x) |> Seq.length = msgSize)

printf "p1: %d\n" ((findFirstMessage input 4) + 4)

printf "p2: %d\n" ((findFirstMessage input 14) + 14)
