﻿open System.IO

let input = File.ReadAllLines "./input"

let inputFolder =
    (fun (state: int[], idx) el ->
        if el = "" then
            (Array.append state [| 0 |], idx + 1)
        else
            state.[idx] <- state[idx] + (el |> int)
            (state, idx))

let (caloriesPerElf, _) = (Array.fold inputFolder ([| 0 |], 0) input)

let sortedCalories = Array.sortByDescending (fun x -> x) caloriesPerElf

printf "p1: %d, p2: %d\n" sortedCalories[0] (sortedCalories[0..2] |> Array.sum)
