module Visibility

let private fromTop (input: int[,]) x y tree =
    Array.where (fun t -> t < tree) input[0 .. y - 1, x] |> Array.length = y

let private fromBottom (input: int[,]) x y tree dim =
    Array.where (fun t -> t < tree) input[y + 1 .. dim, x] |> Array.length = (dim - 1 - y)

let private fromLeft (input: int[,]) x y tree =
    Array.where (fun t -> t < tree) input[y, 0 .. x - 1] |> Array.length = x

let private fromRight (input: int[,]) x y tree dim =
    Array.where (fun t -> t < tree) input[y, x + 1 .. dim] |> Array.length = (dim - 1 - x)

let isTreeVisible x y dim (input: int[,]) =
    if x = 0 || y = 0 || x = dim - 1 || y = dim - 1 then
        true
    else
        let tree = input[y, x]

        (fromLeft input x y tree
         || fromRight input x y tree dim
         || fromTop input x y tree
         || fromBottom input x y tree dim)
