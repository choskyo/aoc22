module Scenery

let rec private dirScore treeUnderConsideration i x (trees: int[]) =
    if i = (Array.length trees) then
        x
    else
        let tree = trees[i]

        let ans = x + 1

        if tree >= treeUnderConsideration then
            ans
        else
            dirScore treeUnderConsideration (i + 1) ans trees

let scenicScore (input: int[,]) x y =
    if x = 0 || y = 0 then
        0
    else
        let upTrees = input[0 .. y - 1, x] |> Array.rev
        let downTrees = input[y + 1 .., x]
        let leftTrees = input[y, 0 .. x - 1] |> Array.rev
        let rightTrees = input[y, x + 1 ..]

        let tree = input[y, x]

        let upScore = dirScore tree 0 0 upTrees
        let downScore = dirScore tree 0 0 downTrees
        let leftScore = dirScore tree 0 0 leftTrees
        let rightScore = dirScore tree 0 0 rightTrees

        upScore * downScore * leftScore * rightScore
