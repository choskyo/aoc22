﻿open System.IO

let input = File.ReadAllLines "./input" |> array2D |> Array2D.map (string >> int)

let inputDimension = Array2D.length1 input

let p1Res =
    input
    |> Array2D.mapi (fun y x _ -> Visibility.isTreeVisible x y inputDimension input)
    |> Seq.cast<bool>
    |> Seq.where (fun s -> s = true)
    |> Seq.length

let p2Res =
    input
    |> Array2D.mapi (fun y x _ -> Scenery.scenicScore input x y)
    |> Seq.cast<int>
    |> Seq.max

printfn "wah"
