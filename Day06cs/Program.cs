﻿var input = File.ReadAllLines("./input").Single();

var buffer = new Queue<char>();
int cursor = 0, startIdx = 0, endIdx = 0;

foreach (var c in input)
{
    buffer.Enqueue(c);

    if (buffer.Count == 15)
    {
        buffer.Dequeue();
    }

    if (buffer.Distinct().Count() == 14 && cursor >= 14)
    {
        startIdx = cursor;
        break;
    }

    cursor++;
}

Console.WriteLine($"p1: {startIdx + 1} | {new string(buffer.ToArray())}");