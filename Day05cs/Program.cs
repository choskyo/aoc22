﻿
using System.Text.RegularExpressions;

var input = File.ReadAllLines("./input");
var splitAt = Array.FindIndex(input, 0, input.Length, x => x?.Length == 0);

var initialState = input.Take(splitAt - 1);
var instructions = input.Skip(splitAt + 1);
var stackCount = int.Parse(input[splitAt - 1].Trim().Last().ToString());

var stacks = Enumerable
    .Range(0, stackCount)
    .Select(_ => new Stack<char>())
    .ToArray();

var initStateRegex = new Regex(@"\[(.*?)\]");

foreach (var row in initialState.Reverse())
{
    var matches = initStateRegex.Matches(row).AsEnumerable();

    for (var i = 0; i < matches.Count(); i++)
    {
        var val = matches.ElementAt(i).Value[1];

        if (val == '.')
        {
            continue;
        }

        var stack = stacks[i];
        stacks[i].Push(val);
    }
}

var instructionRegex = new Regex(@"\d+");

foreach (var row in instructions)
{
    var vals = instructionRegex
        .Matches(row)
        .Select(x => int.Parse(x.Value))
        .ToArray();
    var count = vals[0];
    var from = vals[1];
    var to = vals[2];

    for (var i = 0; i < count; i++)
    {
        var c = stacks[from - 1].Pop();
        stacks[to - 1].Push(c);
    }
}

var p1Res = new string(stacks
    .Select(x => x.Peek())
    .ToArray());

Console.WriteLine($"p1: {p1Res}");

stacks = Enumerable
    .Range(0, stackCount)
    .Select(_ => new Stack<char>())
    .ToArray();

foreach (var row in initialState.Reverse())
{
    var matches = initStateRegex.Matches(row).AsEnumerable();

    for (var i = 0; i < matches.Count(); i++)
    {
        var val = matches.ElementAt(i).Value[1];

        if (val == '.')
        {
            continue;
        }

        var stack = stacks[i];
        stacks[i].Push(val);
    }
}

foreach (var row in instructions)
{
    var vals = instructionRegex
        .Matches(row)
        .Select(x => int.Parse(x.Value))
        .ToArray();
    var count = vals[0];
    var from = vals[1];
    var to = vals[2];

    var buffer = new char[count];

    for (var i = 0; i < count; i++)
    {
        buffer[i] = stacks[from - 1].Pop();
    }

    foreach (var c in buffer.Reverse())
    {
        stacks[to - 1].Push(c);
    }
}

var p2Res = new string(stacks
    .Select(x => x.Peek())
    .ToArray());

Console.WriteLine($"p2: {p2Res}");