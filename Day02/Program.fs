﻿open System.IO

let input = File.ReadAllLines "./input"

let shapeScores =
    Map
        [ ("A X", 4)
          ("A Y", 8)
          ("A Z", 3)
          ("B X", 1)
          ("B Y", 5)
          ("B Z", 9)
          ("C X", 7)
          ("C Y", 2)
          ("C Z", 6) ]

let winScores =
    Map
        [ ("A X", 3)
          ("A Y", 4)
          ("A Z", 8)
          ("B X", 1)
          ("B Y", 5)
          ("B Z", 9)
          ("C X", 2)
          ("C Y", 6)
          ("C Z", 7) ]

let (p1, p2) =
    input
    |> Array.fold (fun (p1, p2) el -> (p1 + shapeScores[el], p2 + winScores[el])) (0, 0)

printf "p1: %d, p2: %d\n" p1 p2
