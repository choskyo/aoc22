﻿open System.IO
open System

let input = File.ReadAllLines "./input"

let ctoi c =
    if c >= 'a' then
        int c - int 'a' + 1
    else
        int c - int 'A' + 27

let splitPack (pack: string) =
    (pack.ToCharArray()) |> Array.splitInto 2 |> Array.map String

let sectGroup (group: string[]) =
    group
    |> Array.map (fun x -> Set.ofArray (x.ToCharArray()))
    |> Set.intersectMany
    |> Array.ofSeq
    |> Array.exactlyOne

let p1Res =
    input |> Array.map (fun x -> ctoi (sectGroup (splitPack x))) |> Array.sum

printf "p1: %d\n" p1Res

let p2Res =
    input
    |> Array.chunkBySize 3
    |> Array.map (fun x -> ctoi (sectGroup x))
    |> Array.sum

printf "p2: %d\n" p2Res
